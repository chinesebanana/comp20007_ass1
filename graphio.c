/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 *
 * Clement Poh (cpoh@unimelb.edu.au)
 *
 * This module provides all the IO functionality related to graphs.
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "graphio.h"

#define MAX_LINE_LEN 256

/* Loads the graph from input */

void print_vertex(FILE *file, void *vertex, Graph g) {
    if (vertex) {
        int *n = ((List)vertex)->data;
        fprintf(file, " %s ", g->vertices[*n].label);
        print_vertex(file, ((List)vertex)->next, g);
    }
}

Graph load_graph(char *input) {
    /* read a text file, use fgets()*/

    FILE *fp;
    char strn[MAX_LINE_LEN+1];
    assert(strn!=NULL);

    fp = fopen(input, "r");
    if (fp == NULL) {
        printf("Error opening file\n");
        exit(1);
    }

    int n, count=0;

    fgets(strn, MAX_LINE_LEN, fp);
    strtok(strn, "\n"); //remove \n char
    n = atoi(strn);
    Graph graph;
    graph = new_graph(n);
    strn[0] = '\0';

    for (int i=0;i<n;i++) {
        fgets(strn, MAX_LINE_LEN, fp);
        strtok(strn, "\n");

        graph->vertices[i].id = i;
        graph->vertices[i].label = malloc(strlen(strn)+1);
        graph->vertices[i].out = graph->vertices[i].in = NULL;
        strcpy(graph->vertices[i].label,strn);

        strn[0] = '\0';
    }
    while (fgets(strn, MAX_LINE_LEN, fp)) {
        //edges
        int iin, iout; //get the indexes of all the edges
        iin = atoi(strtok(strn, " "));
        iout = atoi(strtok(NULL, " "));

        add_edge(graph, iin, iout);
        count++;
    }
    graph->size = count;

    fclose(fp);
    return graph;
}

/* Prints the graph */
void print_graph(char *output, Graph graph) {
    FILE *f;
    f = fopen(output, "w");
    if (f == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    fprintf(f, "digraph {\n");
    for (int k=0;k<graph->order;k++) {
        fprintf(f, "  %s", graph->vertices[k].label);
        if (graph->vertices[k].out) {
            List l = graph->vertices[k].out;
            fprintf(f, " -> {");
            print_vertex(f, l, graph);

            fprintf(f, "}");
        }
        fprintf(f, "\n");
    }
    fprintf(f, "}\n");

    fclose(f);
}

/* Prints the destination vertex label surrounded by spaces */
void print_vertex_label(FILE *file, void *vertex) {
}

/* Prints the id of a vertex then a newline */
void print_vertex_id(FILE *file, void *vertex) {
    if (vertex)
        fprintf(file, "%d\n", ((Vertex)vertex)->id);
}

/* Returns a sequence of vertices read from file */
List load_vertex_sequence(FILE *file, Graph graph) {
    const char *err_duplicate = "Error: duplicate vertex %d %s\n";
    const char *err_order = "Error: graph order %d, loaded %d vertices\n";
    List list = NULL;
    int id;

    while(fscanf(file, "%d\n", &id) == 1) {
        assert(id >= 0);
        assert(id < graph->order);

        if (!insert_if(id_eq, graph->vertices + id, &list)) {
            fprintf(stderr, err_duplicate, id, graph->vertices[id].label);
            exit(EXIT_FAILURE);
        }
    }

    if (len(list) != graph->order) {
        fprintf(stderr, err_order, graph->order, len(list));
        exit(EXIT_FAILURE);
    }

    return list;
}
