/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 *
 * Joanna Grace Cho Ern LEE (student id.: 710094)
 *
 * This provides all the non-IO functionality related to graphs.
 *
*/
#include <stdlib.h>
#include <assert.h>

#include "graph.h"

/* Returns a pointer to a new graph with order vertices */
Graph new_graph(int order) {
    Graph graph;
    graph = malloc(sizeof(*graph));
    assert(graph!=NULL);

    graph->order = order;
    graph->vertices = malloc(sizeof(*(graph->vertices))*order);
    assert(graph->vertices!=NULL);
    return graph;
}

/* Returns whether aim and vertex are pointing to the same location */
bool ptr_eq(void *aim, void *vertex) {
    if ((int *)aim == (int *)vertex) {
        return true;
    }
    return false;
}

/* Returns whether aim and vertex have the same id */
bool id_eq(void *aim, void *vertex) {
    if (((Vertex)aim)->id == ((Vertex)vertex)->id) {
        return true;
    }
    return false;
}

/* Add the edge from v1 to v2 to graph */
void add_edge(Graph graph, int v1, int v2) {
    //not empty, prepend()
    prepend(&graph->vertices[v1].out, &graph->vertices[v2].id);

    //not empty, prepend()
    prepend(&graph->vertices[v2].in, &graph->vertices[v1].id);
}

/* Free the memory allocated to graph */
void free_graph(Graph graph) {
    assert(graph!=NULL);
    for (int i=0; i<graph->order; i++) {
        if (graph->vertices[i].in) {
            free_list(graph->vertices[i].in);
        }
        if (graph->vertices[i].in) {
            free_list(graph->vertices[i].out);
        }
    }
    free(graph->vertices);
    free(graph);
}
