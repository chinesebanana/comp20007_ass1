/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 *
 * Joanna Grace Cho Ern LEE (student id.: 710094)
 *
 * This module provides all the topological sorting functionality.
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "toposort.h"

/* Recursively visits all the nodes accessible to it */
List visit(Graph g, Vertex v, List topolist, bool pre[], bool post[]) {
    if (pre[v->id]) { //there is a pre-number
        //it is CYCLIC
        fprintf(stderr,"This graph has no topological sort as it is cyclic.\n");
        exit(1);
    }
    pre[v->id] = true; //add pre-number
    if (v->out) { //check if any outgoing edges
        List l = v->out;
        while(l->next) {
            int *n = l->data;
            topolist = visit(g, &(g->vertices[*n]), topolist, pre, post);
            l = l->next;
        }
        int *m = l->data;
        topolist = visit(g, &(g->vertices[*m]), topolist, pre, post);
    }
    post[v->id] = true; //add post-number
    pre[v->id] = false; //remove pre-number

    // check if already in topolist
    if (!(find(id_eq, &(v->id), topolist))) {
        if (topolist) {
            prepend(&topolist, &(v->id));
        } else {
            topolist = push(NULL, &(v->id));
        }
    }
    return topolist;
}

/* Check if graph is already topologically sorted */
bool unvisited(bool post[], int size) {
    for (int i=0; i<size; i++) {
        if (!(post[i])) {
            return true;
        }
    }
    return false;
}

/* Returns a list of topologically sorted vertices using the DFS method */
List dfs_sort(Graph graph) {
    List topolist = NULL;
    int n = graph->order;
    // Initialise boolean arrays since nothing is visited yet,
    bool pre[n];
    bool post[n];
    for (int i=0; i<n; i++) {
        pre[i] = false;
        post[i] = false;
    }

    int i = 0;
    while(unvisited(post, n)) {
        if (!(post[i])) { //not visited yet
            topolist = visit(graph, &(graph->vertices[i]), topolist, pre, post);
        }
        i++;
        if (i==(graph->order)) {
            i = 0;
        }
    }
    return topolist;
}

/* Returns a list of nodes with no incoming edges */
List find_sources(Graph graph) {
    List sources = NULL;
    for (int i=0; i<(graph->order); i++) {
        if (!(graph->vertices[i].in)) {
            if (sources) {
                prepend(&sources, &graph->vertices[i].id);
            } else {
                sources = push(NULL, &graph->vertices[i].id);
            }
        }
    }
    return sources;
}

/* Removes edge from graph */
void remove_edge(Graph g, int v1, int v2) {
    /* v1 is host, v2 is outgoing edge */
    pop(&(g->vertices[v1].out));
    pop(&(g->vertices[v2].in));
}

/* Returns a list of topologically sorted vertices using the Kahn method */
List kahn_sort(Graph graph) {
    List topolist = NULL;
    List sources = find_sources(graph); //function to find all sources
    int count = 1; //check if cyclic
    while (sources) {
        int *v = pop(&sources); //id of a source vertex

        if (topolist) {
            prepend(&topolist, v);
        } else {
            topolist = push(NULL, v);
        }
        if (graph->vertices[*v].out) { //check if there's any outgoing edges

            while ((graph->vertices[*v].out)) {
                int *w = (graph->vertices[*v].out)->data;
                remove_edge(graph, *v, *w);

                //check if it is a source vertex now
                if (!(graph->vertices[*w].in)) {
                    prepend(&sources, w);
                    count++;
                }
            }
        }
    }
    if (len(topolist)!= graph->order) {
        //there are still unvisited nodes in graph so it is CYCLIC.
        fprintf(stderr,"This graph has no topological sort as it is cyclic.\n");
        exit(1);
    }
    return reverse(topolist);
}


/* Uses graph to verify vertices are topologically sorted */
bool verify(Graph graph, List vertices) {
    while (vertices->next) { //iterate through topolist
        int *n = vertices->data;
        List listin = graph->vertices[*n].in;
        while (listin) {
            int *m = listin->data;
            if (find(ptr_eq, m, vertices->next)) {
                return false;
            }
            listin = listin->next;
        }
        vertices = vertices->next;
    }
    return true;
}
